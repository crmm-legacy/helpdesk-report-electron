import {
    styleBordersLeftWrap,
    styleBordersRightWrap,
    styleBordersRightWrapNumStr,
    styleTitle
} from '@/js/cellStyles.js';

const start = (agregated, ws) => {
    let data = agregated.statsData;
    let generalData = [
        { text: 'Количество заявок', value: agregated.reportsAmount },
        { text: 'Количество "Решенных" заявок', value: data.resolvedReports },
        { text: 'Количество заявок "В процессе решения"', value: data.pendingReports },
        { text: 'Количество заявок "Нерешенных"', value: data.unresolvedReports },
        { text: 'Количество заявок, которые дублировали изначальную заявку пользователя', value: data.duplicatedReports },
        { text: 'Количество возвратов пользователей по той же проблеме, когда проблема не решена', value: data.userReturns }
    ]
    let programsData = [
        { text: 'Количество программ на поддержке', value: agregated.rangingData.courseList.length },
        { text: 'Количество обучающихся', value: agregated.usersAmount }
    ]
    let averageData = [
        { text: 'Среднее количество обращений пользователя до закрытия заявки', value: data.avrUserRequests || 0 },
        { text: 'Среднее количество ответов поддержки до закрытия заявки', value: data.avrSupportResponses || 0 }
    ]
    let offsetRow = 1;
    let columnsAmount = 2;

    if (agregated.oldReportData) {
        generalData[0].old = agregated.oldReportData.reportsAmount;
        generalData[1].old = agregated.oldReportData.resolvedReports;
        generalData[2].old = agregated.oldReportData.pendingReports;
        generalData[3].old = agregated.oldReportData.unresolvedReports;
        generalData[4].old = agregated.oldReportData.duplicatedReports;
        generalData[5].old = agregated.oldReportData.userReturns;
        programsData[1].old = agregated.oldReportData.usersAmount;
        columnsAmount = 3;
    }

    let calcResultValue = (value, old) => {
        let arrowSign = '';
        let result = 0;

        let relativeChange = value * 100 / old || 0;
        relativeChange = (isFinite(relativeChange)) ? relativeChange : 0;
        let changeAmount = (relativeChange) ? relativeChange - 100 : 0;

        result = +(Math.round(changeAmount + "e+2")  + "e-2");

        if (result) {
            if (value > old) {
                arrowSign = '▲';
            } else if (value < old) {
                arrowSign = '▼';
            }
        }

        return (result > 0) ? `${arrowSign} +${result}%` : `${arrowSign} ${result}%`;
    }

    ws.column(1).setWidth(45);
    ws.column(2).setWidth(15);
    ws.column(3).setWidth(15);

    // Первая таблица. Информация по поддержке
    ws.cell(offsetRow, 1, offsetRow, 2, true)
        .string('Общая информация по поддержке')
        .style(styleTitle);

    offsetRow += 2;

    let generalRange = {s: {r: offsetRow, c: 1}, e: {r: offsetRow + generalData.length - 1, c: columnsAmount}}
    for (let R = generalRange.s.r; R <= generalRange.e.r; R++) {
        for (let C = generalRange.s.c; C <= generalRange.e.c; C++) {
            let elem = generalData[R - offsetRow];
            if (C === 1) {
                ws.cell(R, C)
                    .string(elem.text)
                    .style(styleBordersLeftWrap);
            }
            if (C === 2) {
                ws.cell(R, C)
                    .number(elem.value)
                    .style(styleBordersRightWrap);
            }
            if (columnsAmount === 3) {
                if (C === 3) {
                    if (elem.old !== undefined) {
                        ws.cell(R, C)
                            .string(calcResultValue(elem.value, elem.old))
                            .style(styleBordersRightWrapNumStr);
                    } else {
                        ws.cell(R, C)
                            .number(elem.value)
                            .style(styleBordersRightWrapNumStr);
                    }
                }
            }
        }
    }

    offsetRow += generalData.length + 2;

    // Вторая таблица. Информация по программам
    ws.cell(offsetRow, 1, offsetRow, 2, true)
        .string('Общая информация по программам')
        .style(styleTitle);

    offsetRow += 2;

    let programsRange = {s: {r: offsetRow, c: 1}, e: {r: offsetRow + programsData.length - 1, c: columnsAmount}}
    for (let R = programsRange.s.r; R <= programsRange.e.r; R++) {
        for (let C = programsRange.s.c; C <= programsRange.e.c; C++) {
            let elem = programsData[R - offsetRow];
            if (C === 1) {
                ws.cell(R, C)
                    .string(elem.text)
                    .style(styleBordersRightWrap);
            }
            if (C === 2) {
                ws.cell(R, C)
                    .number(elem.value)
                    .style(styleBordersRightWrap);
            }
            if (C === 3 && elem.old !== undefined) {
                ws.cell(R, C)
                    .string(calcResultValue(elem.value, elem.old))
                    .style(styleBordersRightWrap);
            }
        }
    }

    offsetRow += programsData.length + 2;

    // Третяя таблица. Средние показатели
    ws.cell(offsetRow, 1, offsetRow, 2, true)
        .string('Среднестатистические показатели')
        .style(styleTitle);

    offsetRow += 2;

    let averageRange = {s: {r: offsetRow, c: 1}, e: {r: offsetRow + averageData.length - 1, c: 2}}
    for (let R = averageRange.s.r; R <= averageRange.e.r; R++) {
        for (let C = averageRange.s.c; C <= averageRange.e.c; C++) {
            let elem = averageData[R - offsetRow];
            if (C === 1) {
                ws.cell(R, C)
                    .string(elem.text)
                    .style(styleBordersRightWrap);
            } else {
                ws.cell(R, C)
                    .number(elem.value)
                    .style(styleBordersRightWrap);
            }
        }
    }
}

export default {
    start
}