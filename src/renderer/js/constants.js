export const namesConst = {
  INIT: 'Дано',
  SLA: 'SLA',
  RANGING: 'Ранжирование заявок по темам',
  STATS: 'Статистические показатели',
  DASHBOARD: 'Dashboard',
  ARCHIVE: 'Архив'
};

export const statusConst = {
  notStarted: 'Не начато',
  inProgress: 'В процессе',
  completed: 'Успешно',
  failed: 'Не выполнено'
};