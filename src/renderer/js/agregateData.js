let SLAData = {};
let rangingData = {}
let statsData = {}
let dashboardData = {}
let archiveData = {}

const agregateData = (data) => {
    let agregated = {};
    clearDataVars();

    agregated.reportsAmount = data.length;

    data.forEach(elem => {
        handleSLA(elem);
        handleRanging(elem);
        handleStats(elem);
    })
    processDashboard(data);
    processStats(agregated);
    processRanging(data);

    agregated.SLAData = SLAData;
    agregated.rangingData = rangingData;
    agregated.statsData = statsData;
    agregated.dashboardData = dashboardData;
    agregated.archiveData = archiveData;
    
    return agregated;
}

const handleSLA = (elem) => {
    const checkNormal1 = () => {
        let targets = ['2 линия', 'Нужно передать в АСАНУ', 'Критично'];
        let specialCondition = !targets.some(target => elem.tags.includes(target));

        const checkFirstSLA = () => {
            if (specialCondition && getWorkTime(elem, 'firstSLA') >= SLAData.times.normalSLAFirstAnswer) {
                SLAData.normal1.firstSLA++;
                setResolveEnv(elem, 'SLA');
            }
        }
        const checkFirstNotSLA = () => {
            if (specialCondition && getWorkTime(elem, 'firstNotSLA') < SLAData.times.normalSLAFirstAnswer) {
                SLAData.normal1.firstNotSLA++;
                setResolveEnv(elem, 'notSLA');
            }
        }
        const checkSolutionSLA = () => {
            if (specialCondition && getWorkTime(elem, 'solutionSLA') <= SLAData.times.normalSLASolution) {
                SLAData.normal1.solutionSLA++;
                setResolveEnv(elem, 'SLA');
            }
        }
        const checkSolutionNotSLA = () => {
            if (specialCondition && getWorkTime(elem, 'solutionNotSLA') > SLAData.times.normalSLASolution) {
                SLAData.normal1.solutionNotSLA++;
                setResolveEnv(elem, 'notSLA');
            }
        }
        const checkTotal = () => {
            if (specialCondition) {
                SLAData.normal1.total++;
            }
        }

        checkFirstSLA();
        checkFirstNotSLA();
        checkSolutionSLA();
        checkSolutionNotSLA();
        checkTotal();
    }

    const checkNormal2 = () => {
        let targets = ['Нужно передать в АСАНУ'];
        let specialCondition = targets.some(target => elem.tags.includes(target));

        const checkFirstSLA = () => {
            if (specialCondition && getWorkTime(elem, 'firstSLA') >= SLAData.times.normalSLAFirstAnswer) {
                SLAData.normal2.firstSLA++;
                setResolveEnv(elem, 'SLA');
            }
        }
        const checkFirstNotSLA = () => {
            if (specialCondition && getWorkTime(elem, 'firstNotSLA') < SLAData.times.normalSLAFirstAnswer) {
                SLAData.normal2.firstNotSLA++;
                setResolveEnv(elem, 'notSLA');
            }
        }
        const checkSolutionSLA = () => {
            if (specialCondition && getWorkTime(elem, 'solutionSLA') <= SLAData.times.normalSLASolution) {
                SLAData.normal2.solutionSLA++;
                setResolveEnv(elem, 'SLA');
            }
        }
        const checkSolutionNotSLA = () => {
            if (specialCondition && getWorkTime(elem, 'solutionNotSLA') > SLAData.times.normalSLASolution) {
                SLAData.normal2.solutionNotSLA++;
                setResolveEnv(elem, 'notSLA');
            }
        }
        const checkTotal = () => {
            if (specialCondition) {
                SLAData.normal2.total++;
            }
        }

        checkFirstSLA();
        checkFirstNotSLA();
        checkSolutionSLA();
        checkSolutionNotSLA();
        checkTotal();
    }

    const checkNormal3 = () => {
        let targets = ['2 линия'];
        let notTargets = ['Асана'];
        let specialCondition = targets.some(target => elem.tags.includes(target)) && 
                               notTargets.some(notTarget => !elem.tags.includes(notTarget));

        const checkFirstSLA = () => {
            if (specialCondition && getWorkTime(elem, 'firstSLA') >= SLAData.times.normalSLAFirstAnswer) {
                SLAData.normal3.firstSLA++;
                setResolveEnv(elem, 'SLA');
            }
        }
        const checkFirstNotSLA = () => {
            if (specialCondition && getWorkTime(elem, 'firstNotSLA') < SLAData.times.normalSLAFirstAnswer) {
                SLAData.normal3.firstNotSLA++;
                setResolveEnv(elem, 'notSLA');
            }
        }
        const checkSolutionSLA = () => {
            if (specialCondition && getWorkTime(elem, 'solutionSLA') <= SLAData.times.normalSLASolution) {
                SLAData.normal3.solutionSLA++;
                setResolveEnv(elem, 'SLA');
            }
        }
        const checkSolutionNotSLA = () => {
            if (specialCondition && getWorkTime(elem, 'solutionNotSLA') > SLAData.times.normalSLASolution) {
                SLAData.normal3.solutionNotSLA++;
                setResolveEnv(elem, 'notSLA');
            }
        }
        const checkTotal = () => {
            if (specialCondition) {
                SLAData.normal3.total++;
            }
        }

        checkFirstSLA();
        checkFirstNotSLA();
        checkSolutionSLA();
        checkSolutionNotSLA();
        checkTotal();
    }

    const checkHigh = () => {
        let targets = ['Критично'];
        let specialCondition = targets.some(target => elem.tags.includes(target));

        const checkFirstSLA = () => {
            if (specialCondition && getWorkTime(elem, 'firstSLA') >= SLAData.times.highSLAFirstAnswer) {
                SLAData.high.firstSLA++;
                setResolveEnv(elem, 'SLA');
            }
        }
        const checkFirstNotSLA = () => {
            if (specialCondition && getWorkTime(elem, 'firstNotSLA') < SLAData.times.highSLAFirstAnswer) {
                SLAData.high.firstNotSLA++;
                setResolveEnv(elem, 'notSLA');
            }
        }
        const checkSolutionSLA = () => {
            if (specialCondition && getWorkTime(elem, 'solutionSLA') <= SLAData.times.highSLASolution) {
                SLAData.high.solutionSLA++;
                setResolveEnv(elem, 'SLA');
            }
        }
        const checkSolutionNotSLA = () => {
            if (specialCondition && getWorkTime(elem, 'solutionNotSLA') > SLAData.times.highSLASolution) {
                SLAData.high.solutionNotSLA++;
                setResolveEnv(elem, 'notSLA');
            }
        }
        const checkTotal = () => {
            if (specialCondition) {
                SLAData.high.total++;
            }
        }

        checkFirstSLA();
        checkFirstNotSLA();
        checkSolutionSLA();
        checkSolutionNotSLA();
        checkTotal();
    }

    const getWorkTime = (elem, type) => {
		const calculateWorkTime = (startTime, endTime) => {
			let firstDayEnd = new Date(+startTime);
			let lastDayStart = new Date(+endTime);
			firstDayEnd.setHours(limits.end.hours);
			firstDayEnd.setMinutes(limits.end.minutes);
			lastDayStart.setHours(limits.start.hours);
			lastDayStart.setMinutes(limits.start.minutes);
			
			let firstDayWorkTime = firstDayEnd.getTime() - startTime.getTime();
			let lastDayWorkTime = endTime.getTime() - lastDayStart.getTime();
			let totalWorkTime = firstDayWorkTime + lastDayWorkTime + getDaysWorkTime();
			
			// переводим в часы
			return totalWorkTime / 1000 / 60 / 60;
		}
		
		const getDaysWorkTime = () => {
			let tempStart = new Date(null, null, null, limits.start.hours, limits.start.minutes, 0, 0);
			let tempEnd = new Date(null, null, null, limits.end.hours, limits.end.minutes, 0, 0);
			let daysWorkTime = (timeInfo.daysAmount - 2 - timeInfo.weekendDays) * (tempEnd - tempStart);
			return daysWorkTime;
        }
        
        const convertToMs = (a,b,c) => {
            return c = 0,
            a = a.split('.'),
            !a[1] || (c += a[1] * 1),
            a = a[0].split(':'), b = a.length,
            c += (b == 3 ? a[0] * 3600 + a[1] * 60 + a[2] * 1 : b == 2 ? a[0] * 60 + a[1] * 1 : s = a[0] * 1) * 1e3,
            c
        }

		const getTimeInfo = (start, end) => {
			let tempStart = start;
			let tempEnd = end;
			let weekendDays = 0;
			let daysAmount = 1;
			let dayMilliseconds = 1000 * 60 * 60 * 24;
			
			let d = Math.abs(end.getTime() - start.getTime()) / 1000;
			let r = {};
			let s = {
				year: 31536000,
				month: 2592000,
				week: 604800,
				day: 86400,
				hour: 3600,
				minute: 60,
				second: 1
			};

			while (tempStart <= tempEnd) {
				let day = tempStart.getDay();
				daysAmount++;
				if (day == 0 || day == 6) {
					weekendDays++;
				}
				tempStart = new Date(+tempStart + dayMilliseconds);
			}

			Object.keys(s).forEach(function(key) {
				r[key] = Math.floor(d / s[key]);
				d -= r[key] * s[key];
			});
			r.daysAmount = daysAmount;
			r.weekendDays = weekendDays;
			
			return r;
        }

        let excelEpoch = new Date('1899-12-30T00:00:00');
        let startTime = new Date(elem.openDate);
        let endTime = null;
        let limits = {
			start: { hours: 9, minutes: 0 },
			end: { hours: 18, minutes: 0 }
        };

        if (type === 'firstSLA' || type === 'firstNotSLA') {
            let tempEnd = new Date(elem.firstAnswerTime);
            let tempTimestamp = tempEnd.getTime() - excelEpoch.getTime();
            endTime = new Date(startTime.getTime() + tempTimestamp);
        }
        if (type === 'solutionSLA' || type === 'solutionNotSLA') {
            if (elem.resolveTime.includes('-')) {
                let tempEnd = new Date(elem.resolveTime);
                let tempTimestamp = tempEnd.getTime() - excelEpoch.getTime();
                endTime = new Date(startTime.getTime() + tempTimestamp);
            } else {
                endTime = new Date(startTime.getTime() + convertToMs(elem.resolveTime));
            }
        }
        
        let timeInfo = getTimeInfo(startTime, endTime);
        let workTime = calculateWorkTime(startTime, endTime);
        
        return workTime;
    }

    const setResolveEnv = (elem, env) => {
        if (!elem.resolveEnv) {
            elem.resolveEnv = env;
        }
    }

    checkNormal1();
    checkNormal2();
    checkNormal3();
    checkHigh();
}

const handleRanging = (elem) => {
    let targetCourse = elem.tags.find(tag => tag.startsWith('Курс ')) || 'Другое';

    if (!rangingData.courseList.find(course => course.name === targetCourse)) {
        rangingData.courseList.push({ name: targetCourse, tags: {} })
    }
    let courseElem = rangingData.courseList.find(course => course.name === targetCourse);

    elem.tags.forEach(tag => {
        if (tag.includes('[РБ]') || tag.includes('[SF]')) {
            if (!rangingData.tagList.includes(tag)) {
                rangingData.tagList.push(tag);
            }
            if (!courseElem.tags[tag]) {
                courseElem.tags[tag] = 0;
            }
            courseElem.tags[tag]++;
        }
    })
}

const handleStats = (elem) => {
    if (elem.status === 'Решённый') {
        statsData.resolvedReports++;
    } else {
        if (elem.resolveEnv && elem.resolveEnv === 'SLA') {
            statsData.pendingReports++;
        }
        if (elem.resolveEnv && elem.resolveEnv === 'notSLA') {
            statsData.unresolvedReports++;
        }
    }
    if (!elem.tags.length) {
        statsData.duplicatedReports++;
    }
    if (elem.tags.includes('Проблема не решена')) {
        statsData.userReturns++;
    }
    statsData.totalUserRequests += elem.userRequests;
    statsData.totalSupportResponses += elem.operatorResponses;
}

const processDashboard = (data) => {
    let tagTotals = [];
    let topList = [];

    // Записываем в tagTotals общее кол-во заявок по каждому тэгу
    rangingData.tagList.forEach(tag => {
        let tempElem = { tag: tag, value: 0 };

        rangingData.courseList.forEach(course => {
            tempElem.value += course.tags[tag] || 0;
        })

        tagTotals.push(tempElem);
    })

    // Выделяем три самых популярных тэга
    tagTotals = tagTotals.sort((a, b) => b.value - a.value).slice(0, 3);

    // Формируем топлист из топ-3 тэгов
    tagTotals.forEach(elem => {
        topList.push({
            tag: elem.tag,
            reportsAmount: elem.value,
            resolvedReports: 0,
            top3courses: []
        })
    })
    
    // Определяем у каких 3 курсов больше всего репортов по топ-3 тэгам
    topList.forEach(elem => {
        let targetTag = elem.tag;
        let tempArr = [];

        rangingData.courseList.forEach(course => {
            tempArr.push(Object.create(course));
        })
        tempArr.sort((a, b) => (b.tags[targetTag] || 0) - (a.tags[targetTag] || 0));
        elem.top3courses = tempArr.slice(0, 3);
    })

    data.forEach(report => {
        topList.forEach(topListElem => {
            if (report.tags.includes(topListElem.tag)) {
                if (report.status === 'Решённый') {
                    topListElem.resolvedReports++;
                }
                topListElem.top3courses.forEach(course => {
                    if (report.courseName === course.name) {
                        if (!course.resolved) {
                            course.resolved = course.unresolved = 0;
                        }
                        (report.status === 'Решённый') ? course.resolved++ : course.unresolved++;
                    }
                })
            }
        })
    })
    
    dashboardData.topList = topList;
    dashboardData.tagTotals = tagTotals;
}

const processStats = (agregated) => {
    statsData.avrUserRequests = +(Math.round(statsData.totalUserRequests / agregated.reportsAmount + "e+2") + "e-2");
    statsData.avrSupportResponses = +(Math.round(statsData.totalSupportResponses / agregated.reportsAmount + "e+2") + "e-2");
}

const processRanging = (data) => {
    let otherCourses = rangingData.courseList.shift();
    rangingData.courseList.push(otherCourses);

    // Добавление колонки 'Всего'
    const createTotal = () => {
        let totalCourses = { name: 'Всего', tags: {} }
    
        rangingData.tagList.forEach(tag => {
            rangingData.courseList.forEach(course => {
                if (!totalCourses.tags[tag]) {
                    totalCourses.tags[tag] = 0;
                }
                if (course.tags[tag]) {
                    totalCourses.tags[tag] += course.tags[tag];
                }
            })
        })
    
        rangingData.courseList.unshift(totalCourses);
    }

    const sortTagList = () => {
        // Ресортируем tagList на основе данных из колонки 'Всего' (от большего к меньшему)
        let tagList = rangingData.tagList;
        let totalCourses = rangingData.courseList.find(course => course.name === 'Всего');
        let tempTagList = [];

        for (let key in totalCourses.tags) {
            let tempElem = { name: key, value: totalCourses.tags[key] }
            tempTagList.push(tempElem);
        }

        tempTagList.sort((a, b) => {
            return b.value - a.value;
        })

        // Очищение теглиста без потери указателя
        tagList.splice(0, tagList.length);

        tempTagList.forEach(e => {
            tagList.push(e.name);
        })
    }

    createTotal();
    sortTagList();
}

const clearDataVars = () => {
    let initSLAData = {
        times: {
            normalSLAFirstAnswer: 4,
            normalSLASolution: 96,
            highSLAFirstAnswer: 2,
            highSLASolution: 24
        },
        normal1: {
            firstSLA: 0,
            firstNotSLA: 0,
            solutionSLA: 0,
            solutionNotSLA: 0,
            total: 0
        },
        normal2: {
            firstSLA: 0,
            firstNotSLA: 0,
            solutionSLA: 0,
            solutionNotSLA: 0,
            total: 0
        },
        normal3: {
            firstSLA: 0,
            firstNotSLA: 0,
            solutionSLA: 0,
            solutionNotSLA: 0,
            total: 0
        },
        high: {
            firstSLA: 0,
            firstNotSLA: 0,
            solutionSLA: 0,
            solutionNotSLA: 0,
            total: 0
        }
    };
    
    let initRangingData = {
        tagList: [],
        courseList: [
            { name: 'Другое', tags: {} }
        ]
    }
    
    let initStatsData = {
        resolvedReports: 0,
        pendingReports: 0,
        unresolvedReports: 0,
        duplicatedReports: 0,
        userReturns: 0,
        totalUserRequests: 0,
        avrUserRequests: 0,
        totalSupportResponses: 0,
        avrSupportResponses: 0
    }
    
    let initDashboardData = {
        topList: [],
        tagTotals: []
    }

    let initArchiveData = {}

    SLAData = initSLAData;
    rangingData = initRangingData;
    statsData = initStatsData;
    dashboardData = initDashboardData;
    archiveData = initArchiveData;
}

export default agregateData;