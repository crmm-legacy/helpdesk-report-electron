import {
    styleCentralBorders,
    styleTitleBorders,
    styleBordersLeftWrap,
    styleCourseName,
    styleBordersFillTopElemLeft,
    styleBordersFillTopElemCenter
} from '@/js/cellStyles.js';

const start = (agregated, ws) => {
    let { courseList, tagList } = agregated.rangingData;
    let { topList } = agregated.dashboardData;

    // Заголовки
    ws.cell(1, 1, 2, 1, true)
        .string('Тема/Курс')
        .style(styleTitleBorders);
    ws.cell(1, 2, 1, courseList.length + 1, true)
        .string('Количество')
        .style(styleTitleBorders);
    ws.row(1).setHeight(25);
    ws.column(1).setWidth(45);
    ws.column(2).setWidth(25);

    let tagNamesRange = {s: {c: 1, r: 2}, e: {c: courseList.length + 1, r: tagList.length + 2}}
    for (let R = tagNamesRange.s.r; R <= tagNamesRange.e.r; R++) {
        for (let C = tagNamesRange.s.c; C <= tagNamesRange.e.c; C++) {
            if (!(C === 1 && R === 2)) {
                let course = courseList[C - 2];
                let tag = tagList[R - 3];
                let cellStyle = styleCentralBorders;

                // Названия курсов
                if (R === 2) {
                    if (C !== 2) {
                        ws.column(C).setWidth(course.name.length);
                        ws.cell(R, C)
                            .string(course.name)
                            .style(styleCourseName);
                    } else {
                        // Колонка Всего
                        ws.cell(R, C)
                            .string(course.name)
                            .style(styleTitleBorders);
                    }
                }
                // Боковые тэги
                if (C === 1) {
                    cellStyle = (R === 3 || R === 4 || R === 5) ? styleBordersFillTopElemLeft : styleBordersLeftWrap;

                    ws.cell(R, C)
                        .string(tag)
                        .style(cellStyle)
                }
                // Значения
                if (C !== 1 && R !== 2) {
                    let value = (course.tags[tag]) ? course.tags[tag] : 0;
                    cellStyle = styleCentralBorders;

                    // Считаем топ ячейки только для именных курсов в первых трех рядах
                    // И закрашиваем их
                    if ((R === 3 || R === 4 || R === 5) && C !== 2) {
                        let topCourse = topList.find(e => e.tag === tag).top3courses[0];
                        if (course.name === topCourse.name) {
                            cellStyle = styleBordersFillTopElemCenter;
                        }
                    }

                    ws.cell(R, C)
                        .number(value)
                        .style(cellStyle)
                }
            }
        }
    }
}

export default {
    start
}