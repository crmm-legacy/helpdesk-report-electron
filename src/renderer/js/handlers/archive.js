import {
    styleBordersLeftWrap,
    styleBordersRightWrap,
    styleArchiveHeader
} from '@/js/cellStyles.js';

const start = (agregated, ws) => {
    const newReportDate = () => {
        let now = new Date();
        let newReportDate = `${now.getFullYear()}.${now.getMonth() + 1}.${now.getDate()}`;
        if (agregated.oldReportData) {
            return `${agregated.oldReportData.reportDate.replace('-', '.')} - ${newReportDate}`
        } else {
            return `${newReportDate}`
        }
    }

    let statsData = agregated.statsData;
    let archiveData = (agregated.oldReportData) ? agregated.oldReportData.archive : null;

    let generalData = [
        { text: 'Показатели/Дата', value: newReportDate() },
        { text: 'Количество заявок', value: agregated.reportsAmount },
        { text: 'Количество "Решенных" заявок', value: statsData.resolvedReports },
        { text: 'Количество заявок "В процессе решения"', value: statsData.pendingReports },
        { text: 'Количество заявок "Нерешенных"', value: statsData.unresolvedReports },
        { text: 'Количество заявок, которые дублировали изначальную заявку пользователя', value: statsData.duplicatedReports },
        { text: 'Количество возвратов пользователей по той же проблеме, когда проблема не решена', value: statsData.userReturns }
    ]
    
    ws.column(1).setWidth(40);
    ws.row(1).setHeight(35);

    let offsetColumn = 1;

    // Записываем первый столбец с названиями строк
    for (let C = 1; C <= 1; C++) {
        for (let R = 1; R <= generalData.length; R++) {
            let cellStyle = (R === 1) ? styleArchiveHeader : styleBordersLeftWrap;
            ws.cell(R, C)
                .string(generalData[R - 1].text)
                .style(cellStyle);
        }
    }

    offsetColumn += 1;
    
    // Копируем данные из старого архива
    if (archiveData.length) {
        let archiveRange = {s: {c: offsetColumn, r: 1}, e: {c: archiveData[0].length, r: generalData.length}}
        for (let C = archiveRange.s.c; C <= archiveRange.e.c; C++) {
            for (let R = archiveRange.s.r; R <= archiveRange.e.r; R++) {
                ws.column(C).setWidth(12);
                let cellStyle = (R === 1) ? styleArchiveHeader : styleBordersRightWrap;
                let cellData = archiveData[R - 1][C - 1];

                if (R === 1) {
                    ws.cell(R, C)
                        .string(cellData)  // Тип string
                        .style(cellStyle);
                } else {
                    ws.cell(R, C)
                        .number(cellData)  // Тип number
                        .style(cellStyle);
                }
            }
            offsetColumn += 1;
        }
    }

    // Записываем новый столбец с данными текущей таблицы
    for (let C = offsetColumn; C <= offsetColumn; C++) {
        for (let R = 1; R <= generalData.length; R++) {
            ws.column(C).setWidth(12);
                let cellStyle = (R === 1) ? styleArchiveHeader : styleBordersRightWrap;
                let cellData = generalData[R - 1].value;

                if (R === 1) {
                    ws.cell(R, C)
                        .string(cellData)  // Тип string
                        .style(cellStyle);
                } else {
                    ws.cell(R, C)
                        .number(cellData)  // Тип number
                        .style(cellStyle);
                }
        }
    }
}

export default {
    start
}