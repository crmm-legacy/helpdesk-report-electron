import xl from 'excel4node';
import _ from 'lodash';
let wb = new xl.Workbook();

let merge = (...args) => {
  return wb.createStyle(_.merge({}, ...args))
}

let st = {
  alignHorLeft: {
    alignment: { horizontal: ['left'] }
  },
  alignHorRight: {
    alignment: { horizontal: ['right'] }
  },
  alignHorCenter: {
    alignment: { horizontal: ['center'] }
  },
  alignVerTop: {
    alignment: { vertical: ['top'] }
  },
  alignVerCenter: {
    alignment: { vertical: ['center'] }
  },
  alignWrap: {
    alignment: { wrapText: true }
  },
  bold: {
    font: { bold: true }
  },
  courseName: {
    font: { name: 'Consolas', size: 10 }
  },
  numStr: {
    numberFormat: '@',
  },
  borders: {
    border: {
      left: { style: 'thin', color: '#203A43' },
      right: { style: 'thin', color: '#203A43' },
      top: { style: 'thin', color: '#203A43' },
      bottom: { style: 'thin', color: '#203A43' }
    }
  },
  manualInput: {
    fill: { bgColor: '#E2EFDA', fgColor: '#E2EFDA', type: 'pattern', patternType: 'solid' }
  },
  fillLightGray: {
    fill: { bgColor: '#F2F2F2', fgColor: '#F2F2F2', type: 'pattern', patternType: 'solid' }
  },
  fillGray: {
    fill: { bgColor: '#D9D9D9', fgColor: '#D9D9D9', type: 'pattern', patternType: 'solid' }
  },
  fillLightBlue: {
    fill: { bgColor: '#DDEBF7', fgColor: '#DDEBF7', type: 'pattern', patternType: 'solid' }
  },
  fillTopElem: {
    fill: { bgColor: '#D6DCE4', fgColor: '#D6DCE4', type: 'pattern', patternType: 'solid' }
  },
  fillYellow: {
    fill: { bgColor: '#FFF2CC', fgColor: '#FFF2CC', type: 'pattern', patternType: 'solid' }
  },
  mega: {
    alignment: { horizontal: ['left'], vertical: ['top'], wrapText: true },
    border: {
      left: { style: 'thin', color: '#203A43' },
      right: { style: 'thin', color: '#203A43' },
      top: { style: 'thin', color: '#203A43' },
      bottom: { style: 'thin', color: '#203A43' }
    }
  }
}

export const styleCentral = merge(st.alignHorCenter, st.alignVerCenter)
export const styleCentralBorders = merge(st.alignHorCenter, st.alignVerCenter, st.borders)
export const styleTitle = merge(st.alignHorCenter, st.alignVerCenter, st.bold)
export const styleTitleBorders = merge(st.alignHorCenter, st.alignVerCenter, st.bold, st.borders)
export const styleLeft = merge(st.alignHorLeft)
export const styleRight = merge(st.alignHorRight)
export const styleVMiddle = merge(st.alignVerCenter)
export const styleBold = merge(st.bold)
export const styleWrap = merge(st.alignWrap)
export const styleBorders = merge(st.borders)
export const styleBordersLeftWrap = merge(st.alignHorLeft, st.alignVerCenter, st.alignWrap, st.borders)
export const styleBordersLeftWrapBold = merge(st.alignHorLeft, st.alignVerCenter, st.alignWrap, st.borders, st.bold)
export const styleBordersRightWrap = merge(st.alignHorRight, st.alignVerCenter, st.alignWrap, st.borders)
export const styleBordersRightWrapNumStr = merge(st.alignHorRight, st.alignVerCenter, st.alignWrap, st.borders, st.numStr)
export const styleCourseName = merge(st.bold, st.courseName, st.borders)
export const styleManualInput = merge(st.mega, st.manualInput)
export const styleManualInputRight = merge(st.mega, st.manualInput, st.alignHorRight)
export const styleBordersLightGray = merge(st.mega, st.fillLightGray)
export const styleBordersGray = merge(st.mega, st.fillGray)
export const styleBordersLightBlue = merge(st.mega, st.fillLightBlue)
export const styleBordersFillTopElemLeft = merge(st.mega, st.fillTopElem)
export const styleBordersFillTopElemCenter = merge(st.mega, st.fillTopElem, st.alignHorCenter, st.alignVerCenter)
export const styleArchiveHeader = merge(st.mega, st.fillYellow, st.alignHorCenter, st.alignVerCenter, st.bold)