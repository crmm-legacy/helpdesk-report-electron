import {
    styleBorders,
    styleBordersLeftWrap,
    styleBordersLeftWrapBold,
    styleBordersLightGray,
    styleBordersGray,
    styleBordersLightBlue
} from '@/js/cellStyles.js';

const start = (agregated, ws) => {
    let data = agregated.SLAData;

    let priorities = [
        { title: 'Приоритет "Нормальный" (1-я линия)', color: '#009669', content: data.normal1 },
        { title: 'Приоритет "Нормальный" (2-я линия)', color: '#00b261', content: data.normal2 },
        { title: 'Приоритет "Нормальный" (3-я линия)', color: '#0dad18', content: data.normal3 },
        { title: 'Приоритет "Высокий"', color: '#E20338', content: data.high },
    ]

    let rowNames = [
        { title: 'Первый ответ в рамках SLA', valueRef: 'firstSLA' },
        { title: 'Первый ответ вне SLA', valueRef: 'firstNotSLA' },
        { title: 'Решение в рамках SLA', valueRef: 'solutionSLA' },
        { title: 'Решение вне SLA', valueRef: 'solutionNotSLA' },
        { title: 'Всего заявок', valueRef: 'total' }
    ]

    let colNames = [
        '',
        'Количество заявок',
        'Процент'
    ]

    ws.column(1).setWidth(30);
    ws.column(2).setWidth(30);
    ws.column(3).setWidth(15);

    // Заголовки
    ws.cell(1, 1)
        .string('Количество заявок')
        .style(styleBordersLeftWrapBold);
    ws.cell(1, 2)
        .string('Количество обучающихся по всем программам')
        .style(styleBordersLeftWrapBold);
    ws.cell(2, 1)
        .number(agregated.reportsAmount)
        .style(styleBordersLeftWrap);
    ws.cell(2, 2)
        .number(agregated.usersAmount)
        .style(styleBordersLeftWrap);

    let offsetRow = 4;

    priorities.forEach(priority => {
        ws.cell(offsetRow, 1, offsetRow, 3, true)
            .string(priority.title)
            .style({
                alignment: { horizontal: ['center'] },
                font: { bold: true, color: priority.color }
            });

        offsetRow += 2;

        let dataRange = {s: {r: offsetRow, c: 1}, e: {r: offsetRow + rowNames.length, c: 3}}
        for (let R = dataRange.s.r; R <= dataRange.e.r; R++) {
            for (let C = dataRange.s.c; C <= dataRange.e.c; C++) {
                if (!(C === 1 && R === offsetRow)) {
                    // Названия столбцов
                    if (R === offsetRow) {
                        ws.cell(R, C)
                            .string(colNames[C - 1])
                            .style(styleBordersLeftWrapBold);
                    }
                    // Названия строк
                    if (C === 1) {
                        ws.cell(R, C)
                            .string(rowNames[R - offsetRow - 1].title)
                            .style(styleBorders);
                    }
                    // Значения
                    if (C !== 1 && R !== offsetRow) {
                        let arrElem = rowNames[R - offsetRow - 1];
                        let valueRef = arrElem.valueRef;
                        let value = priority.content[valueRef];
                        let style;

                        // Определяем цвет ячеек
                        if (R === offsetRow + 1 || R === offsetRow + 2) {
                            style = styleBordersLightGray
                        } else if (R === offsetRow + 3 || R === offsetRow + 4) {
                            style = styleBordersGray
                        } else {
                            style = styleBordersLightBlue
                        }

                        // Кол-во заявок
                        if (C === 2) {
                            ws.cell(R, C)
                                .number(value)
                                .style(style);
                        }
                        // Процент
                        if (C === 3) {
                            ws.cell(R, C)
                                .number(Math.round(value / priority.content.total * 100) || 0)
                                .style(style);
                        }
                    }
                }
            }
        }
        offsetRow += rowNames.length + 3;
    })
}

export default {
    start
}