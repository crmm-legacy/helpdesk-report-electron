import { styleTitle, styleManualInput, styleBordersLeftWrap } from '@/js/cellStyles.js';

const start = (agregated, ws) => {
    let topList = agregated.dashboardData.topList;

    const createReportName = () => {
        let now = new Date();
        let newReportDate = `${now.getFullYear()}.${now.getMonth() + 1}.${now.getDate()}`;
        if (agregated.oldReportData) {
            return `Отчет ${agregated.oldReportData.reportDate.replace('-', '.')} - ${newReportDate}`
        } else {
            return `Отчет ${newReportDate}`
        }
    }

    let topListData = [
        'Количество заявок',
        'Из них решенных заявок',
        'Решена ли проблема в целом',
        'Топ 3 курса по указанной теме',
        'Количество решенных/нерешенных заявок по курсу 1',
        'Количество решенных/нерешенных заявок по курсу 2',
        'Количество решенных/нерешенных заявок по курсу 3'
    ]
    let supportStatsData = [
        { text: 'Количество заявок', value: agregated.reportsAmount },
        { text: 'Количество заявок со статусом "Решенная"', value: agregated.statsData.resolvedReports },
        { text: 'Количество заявок со статусом "В процессе решения" (в рамках SLA)', value: agregated.statsData.pendingReports },
        { text: 'Количество заявок с статусом "Нерешенные" (за пределами SLA)', value: agregated.statsData.unresolvedReports }
    ]
    let offsetRow = 4;
    let reportName = createReportName();

    ws.row(1).setHeight(40);
    ws.column(1).setWidth(35);
    ws.column(2).setWidth(35);
    ws.column(3).setWidth(35);
    ws.column(4).setWidth(35);

    ws.cell(1, 2, 1, 3, true)
        .string(reportName)
        .style(styleTitle);

    ws.cell(offsetRow, 1, offsetRow, 4, true)
        .string('Топ 3 темы по которым обращаются пользователи')
        .style(styleTitle);

    offsetRow += 2;

    let topListRange = {s: {r: offsetRow, c: 1}, e: {r: offsetRow + topListData.length, c: 4}}
    for (let R = topListRange.s.r; R <= topListRange.e.r; R++) {
        for (let C = topListRange.s.c; C <= topListRange.e.c; C++) {
            if (!(C === 1 && R === offsetRow)) {
                // Названия столбцов
                if (R === offsetRow) {
                    ws.cell(R, C)
                        .string(topList[C - 2].tag)
                        .style(styleBordersLeftWrap);
                }
                // Названия строк
                if (C === 1) {
                    ws.cell(R, C)
                        .string(topListData[R - offsetRow - 1])
                        .style(styleBordersLeftWrap)
                }
                // Значения
                if (C !== 1 && R !== offsetRow) {
                    let target = topList[C - 2];
                    
                    if (R === offsetRow + 1) {
                        ws.cell(R, C)
                            .number(target.reportsAmount)
                            .style(styleBordersLeftWrap)
                    }
                    if (R === offsetRow + 2) {
                        ws.cell(R, C)
                            .number(target.resolvedReports)
                            .style(styleBordersLeftWrap)
                    }
                    if (R === offsetRow + 3) {
                        ws.cell(R, C)
                            .string('')
                            .style(styleManualInput)
                    }
                    if (R === offsetRow + 4) {
                        let mergedCourseNames = '';
                        target.top3courses.forEach(course => {
                            mergedCourseNames += course.name + ', '
                        })
                        mergedCourseNames = mergedCourseNames.slice(0, -2);

                        ws.cell(R, C)
                            .string(mergedCourseNames)
                            .style(styleBordersLeftWrap)
                    }
                    if (R === offsetRow + 5) {
                        let course = target.top3courses[0];
                        ws.cell(R, C)
                            .string(`${course.resolved}/${course.unresolved}`)
                            .style(styleBordersLeftWrap)
                    }
                    if (R === offsetRow + 6) {
                        let course = target.top3courses[1];
                        ws.cell(R, C)
                            .string(`${course.resolved}/${course.unresolved}`)
                            .style(styleBordersLeftWrap)
                    }
                    if (R === offsetRow + 7) {
                        let course = target.top3courses[2];
                        ws.cell(R, C)
                            .string(`${course.resolved}/${course.unresolved}`)
                            .style(styleBordersLeftWrap)
                    }
                }
            }
        }
    }

    offsetRow += topListData.length + 3;

    ws.cell(offsetRow, 1, offsetRow, 4, true)
        .string('Статистика работы поддержки')
        .style(styleTitle);

    offsetRow += 2;

    let supportStatsRange = {s: {r: offsetRow, c: 1}, e: {r: offsetRow + 1, c: 4}}
    for (let R = supportStatsRange.s.r; R <= supportStatsRange.e.r; R++) {
        for (let C = supportStatsRange.s.c; C <= supportStatsRange.e.c; C++) {
            // Названия столбцов
            if (R === offsetRow) {
                ws.cell(R, C)
                    .string(supportStatsData[C - 1].text)
                    .style(styleBordersLeftWrap);
            }
            // Значения
            else {
                ws.cell(R, C)
                    .number(supportStatsData[C - 1].value)
                    .style(styleBordersLeftWrap);
            }
        }
    }

    offsetRow += 4;

    ws.cell(offsetRow, 2, offsetRow, 3, true)
        .string('Выводы')
        .style(styleTitle);

    offsetRow += 1;

    ws.cell(offsetRow, 2, offsetRow + 8, 3, true)
        .style(styleManualInput);
}

export default {
    start
}